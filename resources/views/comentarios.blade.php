<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/shards-ui/3.0.0/css/shards.css">
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <title>Sanitize</title>
</head>

<body>
    <section id="comentarios" class="pb-4 information">
        <div class="col-8 m-auto">
            <h3 class="my-4">Comentarios</h3>
            <hr>
            <p>
                Queremos conocer tu opinión
            </p>
            <form class="col-6 m-auto" method="POST" action="{{ route('data.store') }}">
            @if (count($errors) > 0)
            <div class="text-danger border-danger border p-3 my-3">
                @foreach ($errors->all() as $error)
                <div>{{ $error }} </div>
                @endforeach
            </div>
            @endif
                @csrf
                <div class="form-group">
                    <label for="email">Dirección de correo</label>
                    <input type="text" name="email" class="form-control" placeholder="Dirección de correo" value="{{old('email')}}">
                    <small class="form-text text-muted">No compartiremos tu dirección con nadie</small>
                </div>
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input name="nombre" type="text" class="form-control" placeholder="Dirección de correo" value="{{old('nombre')}}">
                    <small class="form-text text-muted">Puede ser visto por los demas usuarios</small>
                </div>
                <div class="form-group">
                    <label for="comentario">Comentario</label>
                    <textarea name="comentario" class="form-control" id="comentario">{{old('comentario')}}</textarea>
                </div>
                <button type="submit" class="mt-4 w-100 btn btn-primary">ENVIAR</button>
            </form>

        </div>
    </section>
</body>

</html>