<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/shards-ui/3.0.0/css/shards.css">
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <title>Sanitize</title>
</head>

<body>
    <section class="bg-cover" id="inicio">
        <nav class="navbar navbar-expand-lg navbar-dark">
            <a class="navbar-brand" href="#">Sanitize</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#inicio">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#importancia">Importancia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/comentarios">Comentarios</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="d-flex justify-content-center align-items-center" style="height: 85vh">
            <div class="bg-dark p-4 rounded">
                <h1 class=" text-light font-weight-lighter" style="letter-spacing: 0.4em;">SANITIZACIÓN</h1>
                <hr class="border-light">
                <h2 class=" text-secondary font-weight-light" style="letter-spacing: 0.2em;">CODIV-19</h2>
            </div>
        </div>
    </section>
    <section id="importancia" class="py-5 information">
        <div class="col-8 m-auto">
            <hr>
            <h3 class="my-4">Importancia</h3>
            <hr>
            <p>
                La protección frente al Coronavirus es el reto al que nos enfrentamos para volver a reemprender nuestras
                actividades y
                nuestros negocios. El distanciamiento social y las medidas de higiene extremas van a ser cruciales para
                mantener la
                curva de contagios en descenso hasta su total desaparición y prevenir nuevos brotes en el futuro. La
                limpieza sanitaria
                y las desinfecciones profesionales formarán parte de la rutina de todos los centros de trabajo y
                espacios
                públicos
                después de la COVID-19.
            </p>
            <p>
                La pandemia de la COVID-19 ha puesto en jaque al sistema sanitario y a la economía a nivel mundial. Las
                medidas de
                confinamiento adoptadas para no colapsar los hospitales se van suavizando a medida que decrece el número
                de
                infectados.
            </p>
            <p>
                Lentamente volvemos a lo que se está llamando “la nueva normalidad”. Una normalidad que no volverá a ser
                igual mientras
                exista el riesgo de rebrote de la enfermedad. Las mascarillas serán de uso obligatorio en espacios
                públicos,
                tendremos
                que dejar un espacio de seguridad entre las personas y será imprescindible realizar limpiezas y
                desinfecciones
                periódicas de los espacios susceptibles de tener un gran tránsito de clientes.
            </p>
        </div>
    </section>
    <section class="footer bg-dark text-secondaty">
        <div class="py-4 col-8 m-auto">
            Sanitize | 2021
        </div>
    </section>
</body>

</html>