<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Descripción

Práctica de implementación de __Mecanismos de seguridad__ dentro de una aplicacion de __Laravel__

## Integrantes

- Cano Santiago David
- Cortez Madrid Fabiola
- Mejía Pérez Victor
- Pérez Lucario Juan Daniel

## Notas
Requiere conecxión constante a internet