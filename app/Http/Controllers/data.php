<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\validator;

class data extends Controller
{
    public function mostrar(){
        return view ('comentarios');
    }
    public function store(validator $request){
        $token=$request->input('_token');

        $cleaned_email = strip_tags($request->input('email'));
        $cleaned_nombre = strip_tags($request->input('nombre'));
        $cleaned_comentario = strip_tags($request->input('comentario'));

        $request->replace(['_token'=>$token,'email'=>$cleaned_email, 'nombre'=>$cleaned_nombre, 'comentario'=>$cleaned_comentario]);

        return $request->all();
    }
}
